class ChangeCategorytypeInArticle < ActiveRecord::Migration
  def self.up
  	change_column :articles, :category, :integer
	
  end

  def self.down
	change_column :articles, :category, :string
  end
end
