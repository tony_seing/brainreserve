class AddIsdisplayedToTopics < ActiveRecord::Migration
  def self.up
    add_column :topics, :isdisplayed, :boolean
  end

  def self.down
    remove_column :topics, :isdisplayed, :boolean
  end
end
