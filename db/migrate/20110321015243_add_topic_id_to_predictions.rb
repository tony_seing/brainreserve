class AddTopicIdToPredictions < ActiveRecord::Migration
  def self.up
      add_column :predictions, :topic_id, :integer
  end

  def self.down
      remove_column :predictions, :topic_id
  end
end
