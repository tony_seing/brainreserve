class SetDefaultValuesInCategories < ActiveRecord::Migration
  def self.up
      Category.create :name => "Straight From Faith"
      Category.create :name => "From The Trend Desk"
      Category.create :name => "The Talent Bank Network"
      Category.create :name => "Predictions"
  end

  def self.down
      Category.create :name => "Straight From Faith"
      Category.create :name => "From The Trend Desk"
      Category.create :name => "The Talent Bank Network"
      Category.create :name => "Predictions"
  end
end
