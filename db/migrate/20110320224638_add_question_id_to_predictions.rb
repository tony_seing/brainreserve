class AddQuestionIdToPredictions < ActiveRecord::Migration
  def self.up
     add_column :predictions, :question_id, :integer
   end

  def self.down
     add_column :predictions, :question_id
  end
end
