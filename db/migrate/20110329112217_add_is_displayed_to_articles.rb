class AddIsDisplayedToArticles < ActiveRecord::Migration
  def self.up
    add_column :articles, :isdisplayed, :boolean
  end

  def self.down
    remove_column :articles, :isdisplayed
  end
end
