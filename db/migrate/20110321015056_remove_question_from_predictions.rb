class RemoveQuestionFromPredictions < ActiveRecord::Migration
  def self.up
    remove_column :predictions, :question_id
  end

  def self.down
    add_column :predictions, :question_id, :integer
  end
end
