class ApplicationController < ActionController::Base
  protect_from_forgery

  def authenticate_brainreserve_expert
    if not (used_signed_in? && is_expert?) then
      # do something....user is a signed-in expert
      redirect_to :home
    end
  end

  def authenticate_brainreserve_regular
     # do something...user is a signed-in regular
     
  end

  
  def is_expert
     current_user.role.title == 'Expert'
  end

  def is_regular
    current_user.role.title == 'Regular'
  end

end
