class CommentsController < ApplicationController

  # GET /comments
  # GET /comments.xml
  def index
    @article = Article.find(params[:article_id])
    @comments = @article.comments.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.xml
  def show
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])

    respond_to do |format|
      format.html # Commentshow.html.erb
      format.xml  { render :xml => @comment }
    end
  end


  def approve
    @article = Article.find(params[:article_id])
   @comment = Comment.find(params[:id])
    if (params[:direction] == 'approve')
      @comment.approved = true

    else
      @comment.approved = false
    end

    if @comment.update_attributes(:approved)
       redirect_to article_comment_path, :action => :index, :notice => 'Comment was approved'
    else
       redirect_to article_comment_path, :action => :index, :notice => 'Comment could not be approved!'
    end


  end

  # GET /comments/new
  # GET /comments/new.xmledit_topic_path(@topic)
  def new
    @article = Article.find(params[:article_id])
    @comment = @article.comments.new

    respond_to do |format|
      format.html  # new.html.erb
      format.xml  { render :xml => @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @article = Article.find(params[:article_id])
   @comment = @article.comments.find(params[:id])
  end

  # POST /commentsrender :action => index"
  # POST /comments.xml
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.new(params[:comment])

    respond_to do |format|
      if @comment.save
        redirect_to article_comments_path(@article), :action => :index, :notice => 'Thank you for your comment'
      else
        redirect_to article_comments_path(@article), :action => :index, :notice => "Can't add comment"
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.xml
  def update
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to(@comment, :notice => 'Comment was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @comment.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.xml
  def destroy
    @topic = Article.find(params[:article_id])
    @comment = @topic.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to(comments_url) }
      format.xml  { head :ok }
    end
  end
end
