class ArticlesController < ApplicationController
  # GET /articles
  # GET /articles.xml

  before_filter :authenticate_user!, :except => [:straightfromfaith, :fromthetrenddesk, :talentbanknetwork, :predictions, :thebrain]
  def index
    # @articles = Article.find(:all, :order => :category_id)
    
    #show articles by category
    @articles_SFF = Article.where(:category_id => 1)
    @articles_FTD = Article.where(:category_id => 2)
    @articles_TBN = Article.where(:category_id => 3)
    @articles_P = Article.where(:category_id => 4)
    
    
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @articles }
    
    end
  end



  def straightfromfaith
     @articles_SFF = Article.where(:category_id => 1, :isdisplayed => true)
     respond_to do |format|
      format.html # 
      format.xml  { render :xml => @article }
    end
  end

  def fromthetrenddesk
     @articles_FTD = Article.where(:category_id => 2, :isdisplayed => true)
     respond_to do |format|
      format.html # 
      format.xml  { render :xml => @article }
    end 
  end
 
  
  def talentbanknetwork
     @articles_TBN = Article.where(:category_id => 3, :isdisplayed => true)
     respond_to do |format|
      format.html # 
      format.xml  { render :xml => @article }
    end
  end
 
  
  def predictions
     @articles_P = Article.where(:category_id => 4, :isdisplayed => true)
      respond_to do |format|
      format.html # 
      format.xml  { render :xml => @article }
    end
  end

  def thebrain
    @articles_SFF = Article.where(:category_id => 1, :isdisplayed => true).limit(5)
    @articles_FTD = Article.where(:category_id => 2, :isdisplayed => true).limit(5)
    @articles_TBN = Article.where(:category_id => 3, :isdisplayed => true).limit(5)
    @articles_P = Article.where(:category_id => 4, :isdisplayed => true).limit(5)
    respond_to do |format|
      format.html # 
      format.xml  { render :xml => @article }
    end
  end

 

  # GET /articles/1
  # GET /articles/1.xml
  def show
    @article = Article.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @article }
    end
  end

  def toggledisplay
    @article = Article.find(params[:id])
    if (params[:direction] == 'display')
      @article.isdisplayed = true
    else
      @article.isdisplayed = false
    end

    if @article.update_attributes(:isdisplayed)
       redirect_to :action => :index
    else
       redirect_to :action => :index, :notice => 'Display could not be changed!'
    end
  end

  # GET /articles/new
  # GET /articles/new.xml
  def new
    @article = Article.new
    @categories = Category.all
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @article }
    end
  end

  # GET /articles/1/edit
  def edit
    @article = Article.find(params[:id])
    @categories = Category.all
    
  end

  # POST /articles
  # POST /articles.xml
  def create
    @article = Article.new(params[:article])

    respond_to do |format|
      if @article.save
        format.html { redirect_to(@article, :notice => 'Article was successfully created.') }
        format.xml  { render :xml => @article, :status => :created, :location => @article }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /articles/1
  # PUT /articles/1.xml
  def update
    @article = Article.find(params[:id])

    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to(@article, :notice => 'Article was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.xml
  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    respond_to do |format|
      format.html { redirect_to(articles_url) }
      format.xml  { head :ok }
    end
  end
end
