class PredictionsController < ApplicationController
  # GET /predictions
  # GET /predictions.xml
  def index
    @topic = Topic.find(params[:topic_id])
    @predictions = @topic.predictions.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @predictions }
    end
  end

  # GET /predictions/1
  # GET /predictions/1.xml
  def show
    @topic = Topic.find(params[:topic_id])
    @prediction = @topic.predictions.find(params[:id])

    respond_to do |format|
      format.html # Predictionshow.html.erb
      format.xml  { render :xml => @prediction }
    end
  end

  
  def approve
    @topic = Topic.find(params[:topic_id])
   @prediction = Prediction.find(params[:id])
    if (params[:direction] == 'approve')
      @prediction.approved = true
      
    else
      @prediction.approved = false
    end
    
    if @prediction.update_attributes(:approved)
       redirect_to :action => :index, :notice => 'Prediction was approved'
    else
       redirect_to :action => :index, :notice => 'Prediction could not be approved!'
    end
  end

  # GET /predictions/new
  # GET /predictions/new.xmledit_topic_path(@topic)
  def new
    @topic = Topic.find(params[:topic_id])
    @prediction = Prediction.new

    respond_to do |format|
      format.html  # new.html.erb
      format.xml  { render :xml => @prediction }
    end
  end

  # GET /predictions/1/edit
  def edit
    @topic = Topic.find(params[:topic_id])
   @prediction = @topic.predictions.find(params[:id])
  end

  # POST /predictionsrender :action => index"
  # POST /predictions.xml
  def create
    @topic = Topic.find(params[:topic_id])
    @prediction = @topic.predictions.new(params[:prediction])

    respond_to do |format|
      if @prediction.save
        format.html { redirect_to(@prediction, :notice => 'Prediction was successfully created.') }
        format.xml  { render :xml => @prediction, :status => :created, :location => @prediction }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @prediction.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /predictions/1
  # PUT /predictions/1.xml
  def update
    @topic = Topic.find(params[:topic_id])
    @prediction = @topic.predictions.find(params[:id])

    respond_to do |format|
      if @prediction.update_attributes(params[:prediction])
        format.html { redirect_to(@prediction, :notice => 'Prediction was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @prediction.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # DELETE /predictions/1
  # DELETE /predictions/1.xml
  def destroy
    @topic = Topic.find(params[:topic_id])
    @prediction = @topic.predictions.find(params[:id])
    @prediction.destroy

    respond_to do |format|
      format.html { redirect_to(predictions_url) }
      format.xml  { head :ok }
    end
  end
end
