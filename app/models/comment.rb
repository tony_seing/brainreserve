class Comment < ActiveRecord::Base
   validates :email, :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i},
                    :presence => true,
                    :length => {:minimum => 3, :maximum => 254}
  validates :content, :presence => true
  validates :name, :presence => true
  belongs_to :article


end
