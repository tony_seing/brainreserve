class Article < ActiveRecord::Base
  has_many :comments
  belongs_to :category
  
  validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 60}
  validates :content, :presence => true
  validates :user, :presence => true
  validates :author, :presence => true
  validates :category_id, :presence => true
end
